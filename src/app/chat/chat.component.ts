import {Component, OnInit, AfterViewChecked, ViewChild, ElementRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../_services/user.service';
import { UserDetail } from '../_classes/user';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewChecked {
  user: UserDetail;
  input = '';
  messages = [];
  @ViewChild('msgContainer') msgContainer: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) {}

  ngOnInit() {
    this.route.params.subscribe((params: {id: number}) => {
      this.userService.getUser(params.id).subscribe(data => {
        this.user = data;
        this.messages = this.userService.getMessages(data.id);
        this.scrollToBottom();
      });
    });
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.msgContainer.nativeElement.scrollTop = this.msgContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  sendMessage() {
    if (this.input.length) {
      this.userService.addMessage(this.user.id, this.input);
      this.messages = this.userService.getMessages(this.user.id);
      this.input = '';
    }
  }
}
