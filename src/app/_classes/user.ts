export class UserList {
  id: number;
  name: string;
}

export class UserDetail {
  id: number;
  name: string;
  age: number;
  city: string;
  male: string;
}
