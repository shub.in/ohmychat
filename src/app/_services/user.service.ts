import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { UserDetail, UserList } from '../_classes/user';

@Injectable()
export class UserService {
  API_URL = './assets/api';
  messages: any;

  constructor(private http: HttpClient) {
    if (localStorage.getItem('messages')) {
      this.messages = JSON.parse(localStorage.getItem('messages'));
    } else {
      localStorage.setItem('messages', JSON.stringify({}));
    }
  }

  getMessages(id) {
    id = Number(id);
    if (id in this.messages) {
      return this.messages[id];
    } else {
      return [];
    }
  }

  addMessage(id, msg) {
    if (id in this.messages) {
      this.messages[id].push(msg);
    } else {
      this.messages[id] = [msg];
    }
    localStorage.setItem('messages', JSON.stringify(this.messages));
  }

  getList(): Observable<UserList[]> {
    const url = `${this.API_URL}/users.json`;
    return this.http.get<UserList[]>(url);
  }

  getUser(id: number): Observable<UserDetail> {
    const url = `${this.API_URL}/${id}.json`;
    return this.http.get<UserDetail>(url);
  }
}
