import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'avatar'
})
export class AvatarPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const separated = value.split(' ');
    if (separated.length > 1) {
      return separated[0][0].toUpperCase() + separated[1][0].toUpperCase();
    } else if (separated.length === 1) {
      return separated[0][0].toUpperCase();
    } else {
      return '-';
    }
  }

}
