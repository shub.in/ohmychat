import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { UserService } from '../_services/user.service';
import { UserList } from '../_classes/user';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit, OnChanges {
  selected: number;
  is_selected = 'inactive';
  is_chat = false;
  users: UserList[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) {
    if (this.route.firstChild != null) {
      this.route.firstChild.params.subscribe((params: {id: number}) => {
        if ('id' in params) {
          this.selected = params.id;
          this.is_selected = 'active';
        }
      });
    }
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.is_chat = e.url.includes('chat');
        if (e.url === '/') { this.selected = null; this.is_selected = 'inactive'; }
      }
    });
  }

  ngOnInit() {
    this.userService.getList().subscribe((data) => {
      this.users = data;
    });
  }

  ngOnChanges() {
    console.log(this.route.firstChild);
  }

  select(id) {
    this.selected = id;
    this.router.navigate(['/user', id]);
  }

  isSelected() {
    return this.is_selected === 'active' ? 'active' : 'inactive';
  }
}
