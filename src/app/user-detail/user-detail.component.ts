import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UserService } from '../_services/user.service';
import { UserDetail } from '../_classes/user';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit{
  user: UserDetail;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) {}

  ngOnInit() {
    this.route.params.subscribe((params: {id: number}) => {
      this.userService.getUser(params.id).subscribe(data => {
        this.user = data;
      });
    });
  }
}
