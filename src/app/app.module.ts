import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';

import { UserService } from './_services/user.service';
import { AvatarPipe } from './_pipes/user.pipe';
import { ChatComponent } from './chat/chat.component';


const routes: Routes = [
  { path: '', component: UserListComponent, children:
    [
      { path: 'user/:id', component: UserDetailComponent},
      { path: 'chat/:id', component: ChatComponent},
    ]
  },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
];


@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserDetailComponent,
    AvatarPipe,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
